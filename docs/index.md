# Diagnostic Dummy HTTP Server

_Dummy HTTP Server for request diagnostics_


## Abstract

Runs a dummy HTTP server from the console
that just logs the incoming requests to localhost port 8000
and answers every request with status code 200 and an empty body,
until the processs is ended eg. by hitting Ctrl-C.

Requests are logged using level INFO, so `-v` or `-d`
is required to see anything.

Supports only the DELETE, GET, POST and PUT HTTP methods.

Example call:

```
$ python3 -m ddhs -vr
INFO     | ===== Running on localhost port 8000 =====
(...)
```


## Usage

Output of `python3 -m ddhs --help`:

```text
usage: ddhs [-h] [--version] [-d | -v | -q] [-r]

Dummy HTTP Server for request diagnostics

options:
  -h, --help     show this help message and exit
  --version      print version and exit
  -r, --run      run the server

Logging options:
  control log level (default is WARNING)

  -d, --debug    output all messages (log level DEBUG)
  -v, --verbose  be more verbose (log level INFO)
  -q, --quiet    be more quiet (log level ERROR)
```

