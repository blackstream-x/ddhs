# Diagnostic Dummy HTTP Server

_Dummy HTTP Server for request diagnostics_


## Usage

Output of `python3 -m ddhs --help`:

```
usage: ddhs [-h] [--version] [-d | -v | -q] [-r]

Dummy HTTP Server for request diagnostics

options:
  -h, --help     show this help message and exit
  --version      print version and exit
  -r, --run      run the server

Logging options:
  control log level (default is WARNING)

  -d, --debug    output all messages (log level DEBUG)
  -v, --verbose  be more verbose (log level INFO)
  -q, --quiet    be more quiet (log level ERROR)
```


## Further reading

Please see the documentation at <https://blackstream-x.gitlab.io/ddhs>
for detailed usage information.

If you found a bug or have a feature suggestion,
please open an issue [here](https://gitlab.com/blackstream-x/ddhs/-/issues)

