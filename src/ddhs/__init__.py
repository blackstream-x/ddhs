# -*- coding: utf-8 -*-

"""

ddhs

Core module


Copyright (C) 2023 Rainer Schwarzbach

This file is part of ddhs.

ddhs is free software: you can redistribute it and/or modify
it under the terms of the MIT License.

ddhs is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the LICENSE file for more details.

"""


__version__ = "0.1.0"


# vim: fileencoding=utf-8 ts=4 sts=4 sw=4 autoindent expandtab syntax=python:
