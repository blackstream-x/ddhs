# -*- coding: utf-8 -*-

"""

ddhs.server

HTTP server functionality


Copyright (C) 2023 Rainer Schwarzbach

This file is part of ddhs.

ddhs is free software: you can redistribute it and/or modify
it under the terms of the MIT License.

ddhs is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the LICENSE file for more details.

"""


import logging
import http.server

from ddhs import __version__


class DiagnosticRequestHandler(http.server.BaseHTTPRequestHandler):

    """Diagnostic HTTP server handler logging all received data"""

    server_version = f"DDHS/{__version__}"

    def __log_request(self):
        """Log the request"""
        logging.info("--- BEGIN incoming request ---")
        logging.info(self.requestline)
        content_length = 0
        for header, content in self.headers.items():
            logging.info("%s: %s", header, content)
            if header.lower() == "content-length":
                content_length = int(content)
            #
        #
        logging.info("")
        if content_length:
            data = self.rfile.read(content_length)
            logging.info(data)
        #
        logging.info("--- END incoming request ---")

    # pylint: disable=invalid-name ; by design

    def do_GET(self):
        """Handle a GET request"""
        self.__log_request()
        self.send_response(200)
        self.end_headers()

    do_POST = do_PUT = do_DELETE = do_GET

    # pylint: enable=invalid-name


def run(
    server_class=http.server.HTTPServer,
    handler_class=DiagnosticRequestHandler,
    address="localhost",
    port=8000,
):
    """Run the local server"""
    logging.info("===== Running on %s port %s =====", address, port)
    server_address = (address, port)
    httpd = server_class(server_address, handler_class)
    httpd.serve_forever()


# vim: fileencoding=utf-8 ts=4 sts=4 sw=4 autoindent expandtab syntax=python:
